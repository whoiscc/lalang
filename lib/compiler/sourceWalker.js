import runtime from '../runtime'
import source from './source'

const walker = runtime.base.Extend({
  Source() {
    return [[], source]
  },
  With() {
     return [[source], this]
  },
  Line() {
    return [[], runtime.number]
  },
  Column() {
    return [[], runtime.number]
  },
  HandlerType() {
    return [[], base]
  },
  OfHandlerType() {
    return [[base], this]
  },
  OnWord() {
    return [[runtime.string], this.HandlerType()]
  },
  OnIndentLevel() {
    return [[runtime.number], this.HandlerType()]
  },
  OnEnd() {
    return [[], this.HandlerType()]
  },
  Walk() {
    return [[], this.HandlerType()]
  }
}, {
  Source: null,
  With(source) {
    return this.Extend({}, {
      Source() {
        return source
      },
      Line() {
        return runtime.number.Native(1)
      },
      Column() {
        return runtime.number.Native(1)
      }
    })
  },
  HandlerType: null,
  OfHandlerType(type) {
    reutrn this.Entend({}, {
      HandlerType() {
        return type
      }
    })
  },
  OnWord: null,
  OnIndentLevel: null,
  OnEnd: null,
})
