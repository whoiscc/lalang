import runtime from '../runtime'

const source = runtime.base.Extend({
  IsEnd() {
    return [[], runtime.boolean]
  },
  Current() {
    return [
      [], runtime.optional, () => [],
      (opt) => [[opt.Type(), runtime.string]]
    ]
  },
  Next() {
    return [[], source]
  }
}, {
  IsEnd: null,
  Current: null,
  Next: null,
})

const stringSource = source.Extend({
  String() {
    return [[], runtime.string]
  },
  With() {
    return [[runtime.string], this]
  },
  Pos() {
    return [[], runtime.number]
  }
}, {
  String: null,
  With(string) {
    return this.Extend({}, {
      String() {
        return string
      },
      Pos() {
        return runtime.number.Native(0)
      }
    })
  },
  Pos: null,
  IsEnd() {
    return runtime.number.Compare().LessThan(
      this.Pos(), this.String().Length()).Not()
  },
  Next() {
    const this_ = this
    return runtime.cond.Of(stringSource).Extend({}, {
      If() {
        return this_.IsEnd().Not()
      },
      Then() {
        return stringSource.Extend({}, {
          String() {
            return this_.String()
          },
          Pos() {
            return this_.Pos().Add(runtime.number.Native(1))
          },
        })
      },
      Else() {
        return this_
      }
    }).Run()
  },
  Current() {
    return this.String().At(this.Pos())
  }
})

export default source
export { stringSource }
