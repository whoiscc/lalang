import runtime from '../runtime'
import { stringSource } from './source'

runtime.test.Init('create a stringSource').Extend({}, {
  Do() {
    this.Expect(stringSource.With(runtime.string.Native('')).IsEnd()).IsTrue()
    this.Expect(stringSource.With(runtime.string.Native('cowsay')).IsEnd())
      .IsFalse()
  }
}).Run()

runtime.test.Init('forward to next position').Extend({}, {
  Do() {
    const source = stringSource.With(runtime.string.Native('cowsay'))
    this.Expect(source.Next().IsEnd()).IsFalse()
    this.Expect(source.Next().Current().Extract(runtime.string.Native('')))
      .ToEqual(runtime.string.Native('o')).With(runtime.string.Equal())
    this.Expect(
      stringSource.With(runtime.string.Native('c')).Next().IsEnd()
    ).IsTrue()
    this.Expect(
      stringSource.With(runtime.string.Native('c')).Next().Next().IsEnd()
    ).IsTrue()
  }
}).Run()
