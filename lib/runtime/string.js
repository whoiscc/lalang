import base from './base'
import equal from './equal'
import { true_, false_ } from './boolean'
import number from './number'
import { default as optional, just, nothing } from './optional'
import cond from './cond'

let equalOfString

const string = base.Extend({
  _Native() {
    return [[], '_any']
  },
  Native() {
    return [['_any'], this]
  },
  Equal() {
    return [[], equal, () => [], (eq) => [[eq.Type(), string]]]
  },
  Slice() {
    return [[number, number], string]
  },
  Length() {
    return [[], number]
  },
  At() {
    return [[number], optional, () => [], (opt) => [[opt.Type(), string]]]
  }
}, {
  _Native: null,
  Native(s) {
    return this.Extend({}, {
      _Native() {
        return s
      }
    })
  },
  Equal() {
    return equalOfString
  },
  Slice(start, end) {
    return string.Native(this._Native().slice(start._N(), end._N()))
  },
  At(pos) {
    const this_ = this
    return cond.Of(optional).Extend({}, {
      If() {
        return number.Compare().LessEqual(number.Native(0), pos).And(
          number.Compare().LessThan(pos, this_.Length()))
      },
      Then() {
        return just.Of(string).WithValue(
          this_.Slice(pos, pos.Add(number.Native(1))))
      },
      Else() {
        return nothing.Of(string)
      }
    }).Run()
  },
  Length() {
    return number.Native(this._Native().length)
  }
})

equalOfString = equal.Of(string).Extend({}, {
  Between(a, b) {
    return a._Native() == b._Native() ? true_ : false_
  }
})
 export default string
