import base from './base/internal'

let equalOfBoolean  // = equal.Of(boolean)

const boolean = base.Extend({
  Equal() {
    return [[], equalOfBoolean]
  },
  And() {
    return [[boolean], boolean]
  },
  Or() {
    return [[boolean], boolean]
  },
  Not() {
    return [[], boolean]
  }
}, {
  Equal() {
    return equalOfBoolean
  },
  And: null,
  Or: null,
  Not: null,
})

const true_ = boolean.Extend({}, {
  And(other) {
    return other
  },
  Or(other) {
    return true_
  },
  Not() {
    return false_
  }
})
const false_ = boolean.Extend({}, {
  And(other) {
    return false_
  },
  Or(other) {
    return other
  },
  Not() {
    return true_
  }
})

import { _placeholder as basePlaceholder } from './base/internal'
basePlaceholder.true_ = true_
basePlaceholder.false_ = false_

import { _placeholder as equalPlaceholder } from './equal/internal'
equalPlaceholder.boolean = boolean

import equal from './equal'
equalOfBoolean = equal.Of(boolean).Extend({}, {
  Between(a, b) {
    return a.Is(b).And(b.Is(a))
  }
})

export default boolean
export { true_, false_ }
