import base from './base'
import { default as boolean, true_ } from './boolean'

const cond = base.Extend({
  If() {
    return [[], boolean]
  },
  Type() {
    return [[], base]
  },
  Then() {
    return [[], this.Type()]
  },
  Else() {
    return [[], this.Type()]
  },
  Run() {
    return [[], this.Type()]
  },
  Of() {
    return [[base], this]
  }
}, {
  If: null,
  Type: null,
  Then: null,
  Else: null,
  Run() {
    return this.If() == true_ ? this.Then() : this.Else()
  },
  Of(type) {
    return this.Extend({}, {
      Type() {
        return type
      }
    })
  }
})

export default cond
