import test from 'ava'
import base from './base'
import { true_, false_ } from './boolean'

test('dummy test', t => {
  t.is(base, base)
})

test('extend base', t => {
  const cowsay = base.Extend({
    Name() {
      return [[], '_any']
    },
  }, {
    Name() {
      return 'cowsay'
    },
  })
  t.deepEqual(cowsay.Name(), 'cowsay')
})

test('extend method uses `this`', t => {
  const cowsay = base.Extend({
    Name() {
      return [[], '_any']
    },
    SayHi() {
      return [[], '_any']
    },
  }, {
    Name() {
      return 'cowsay'
    },
    SayHi() {
      return `Hello, I'm ${this.Name()}.`
    },
  })
  t.deepEqual(cowsay.SayHi(), 'Hello, I\'m cowsay.')
})

test('extend more than once', t => {
  const aPerson = base.Extend({
    Name() {
      return [[], '_any']
    },
    SayHi() {
      return [[], '_any']
    },
  }, {
    Name: null,
    SayHi() {
      return `Hello, I'm ${this.Name()}.`
    }
  })
  const cowsay = aPerson.Extend({}, {
    Name() {
      return 'cowsay'
    }
  })
  t.deepEqual(cowsay.SayHi(), 'Hello, I\'m cowsay.')
})

test('extend in method', t => {
  const person = base.Extend({
    Name() {
      return [[], '_any']
    },
    SayHi() {
      return [[], '_any']
    },
    WithName() {
      return [['_any'], this]
    }
  }, {
    Name: null,
    WithName(name) {
      return this.Extend({}, {
        Name() {
          return name
        }
      })
    },
    SayHi() {
      return `Hello, I'm ${this.Name()}.`
    }
  })
  const cowsay = person.WithName('cowsay')
  t.deepEqual(cowsay.SayHi(), 'Hello, I\'m cowsay.')
})

test('use `Is` to test inheritance', t => {
  t.is(base.Is(base), true_)
  const dummy = base.Extend({}, {})
  t.is(dummy.Is(base), true_)
  t.is(base.Is(dummy), false_)
  const anotherDummy = dummy.Extend({}, {})
  t.is(anotherDummy.Is(dummy), true_)
  t.is(anotherDummy.Is(base), true_)
})
