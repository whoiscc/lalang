import test from 'ava'
import number from './number'
import { true_, false_ } from './boolean'

test('0 equals to 0', t => {
  t.is(number.Equal().Between(number.Native(0), number.Native(0)), true_)
})

test('1 adds 2 equals to 3', t => {
  t.is(
    number.Equal().Between(
      number.Native(1).Add(number.Native(2)),
      number.Native(3)
    ),
    true_
  )
})

test('1 + (-2) = -((-1) + 2)', t => {
  t.is(
    number.Equal().Between(
      number.Native(1).Add(number.Native(2).Neg()),
      number.Native(1).Neg().Add(number.Native(2)).Neg()
    ),
    true_
  )
})

test('1 < 2, 1 <= 1, 1 <= 2', t => {
  t.is(number.Compare().LessThan(number.Native(1), number.Native(2)), true_)
  t.is(number.Compare().LessEqual(number.Native(1), number.Native(1)), true_)
  t.is(number.Compare().LessEqual(number.Native(1), number.Native(2)), true_)
  t.is(number.Compare().LessEqual(number.Native(2), number.Native(1)), false_)
})
