import test from 'ava'
import { default as boolean, true_, false_ } from './boolean'

test('true equals to true and not equals to false', t => {
  t.is(boolean.Equal().Between(true_, true_), true_)
  t.is(boolean.Equal().Between(true_, false_), false_)
  t.is(boolean.Equal().Between(false_, false_), true_)
  t.is(boolean.Equal().Between(false_, true_), false_)
})

test('true and true is true, true or false is true', t => {
  t.is(true_.And(true_), true_)
  t.is(true_.Or(true_), true_)
  t.is(true_.And(false_), false_)
  t.is(true_.Or(false_), true_)
})

test('not true is false', t => {
  t.is(true_.Not(), false_)
  t.is(false_.Not(), true_)
})
