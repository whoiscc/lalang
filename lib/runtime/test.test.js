import test from './test'
import base from './base'
import number from './number'

test.Init('test test could test well').Extend({}, {
  Do() {
    this.Expect(base).ToBe(base)
    this.Expect(test).ToBe(base)
  }
}).Run()

// test.Init('test should fail when it should fail').Extend({}, {
//   Do() {
//     this.Expect(base).ToBe(test)
//   }
// }).Run()

test.Init('test equal').Extend({}, {
  Do() {
    this.Expect(number.Native(42)).ToEqual(number.Native(42))
      .With(number.Equal())
    this.Expect(number.Native(42)).ToNotEqual(number.Native(43))
      .With(number.Equal())
  }
}).Run()
