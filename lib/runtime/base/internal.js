//
function Base() {}

const _ = {}

Base.prototype.Is = function(parent) {
  // cannot use `cond` here, or type checking may cause infinite recursion
  return this instanceof parent.constructor ? _.true_ : _.false_
}

Base.prototype.Extend = function Extend(methodsT, methods) {
  function Extended() {}
  Extended.prototype.__proto__ = this.__proto__

  for (const [name, method] of Object.entries(methods)) {
    function WrappedMethod (...args) {
      const [argsT, retT, argsWhereT, retWhereT] = this[`_T+${name}`]()
      if (argsT.length != args.length) {
        throw new Error(
          `expect ${argsT.length} arguments, passed ${args.length}`)
      }
      for (const i in args) {
        if (argsT[i] != '_any' && args[i].Is(argsT[i]) == _.false_) {
          throw new Error(`argument #${i} has wrong type`)
        }
      }
      if (argsWhereT != undefined) {
        for (const [extended, base] of argsWhereT(...args)) {
          if (extended.Is(base) == _.false_) {
            throw new Error(`argument constraint not satisfy`)
          }
        }
      }
      if (method == null) {
        throw new Error('method is not implemented')
      }
      const ret = method.bind(this)(...args)
      if (retT != '_any' && ret.Is(retT) == _.false_) {
        throw new Error('returned value has wrong type')
      }
      if (retWhereT != undefined) {
        for (const [extended, base] of retWhereT(ret, args)) {
          if (extended.Is(base) == _.false_) {
            throw new Error(`argument constraint not satisfy`)
          }
        }
      }
      return ret
    }
    Extended.prototype[name] = WrappedMethod
  }

  for (const [name, methodT] of Object.entries(methodsT)) {
    if (this[name] != undefined) {
      // whereT is not checked here, because I don't know how to do that
      const [baseArgsT, baseRetT] = this[`_T+${name}`]()
      const [argsT, retT] = methodT.bind(this)()
      if (argsT.length != baseArgsT.length) {
        throw new Error(`override method wrong number of arguments ` +
          `(${argsT.length} != ${baseArgsT.length})`)
      }
      for (const i in argsT) {
        if (argsT[i] != 'any' && baseArgsT[i].Is(argsT[i]) == _.false_) {
          throw new Error(`override method argument #${i} has wrong type`)
        }
      }
      if (baseRetT != 'any' && retT.Is(baseRetT) == _.false_) {
        throw new Error(`override method returned value has wrong type`)
      }
    }
    Extended.prototype[`_T+${name}`] = methodT
  }

  Extended.prototype.constructor = Extended
  Extended.prototype.Super = function() {
    function Super() {}
    Super.prototype = Extended.prototype.__proto__
    for (const [name, method] of Object.entries(Extended.prototype.__proto__)) {
      if (name == 'Super') {
        continue
      }
      Super.prototype[name] = method.bind(this)
    }
    return new Super()
  }
  return new Extended()
}

export default new Base()
export { _ as _placeholder }
