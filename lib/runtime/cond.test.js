import test from 'ava'
import cond from './cond'
import number from './number'
import { true_ } from './boolean'

test('if-else condition', t => {
  const fortyTwo = cond.Of(number).Extend({}, {
    If() {
      return number.Equal().Between(number.Native(0), number.Native(1))
    },
    Then() {
      return number.Native(41)
    },
    Else() {
      return this.Then().Add(number.Native(1))
    }
  }).Run()
  t.is(number.Equal().Between(fortyTwo, number.Native(42)), true_)
})
