import test from 'ava'
import base from './base'
import { true_, false_ } from './boolean'

test('add method without type', t => {
  const tell = base.Extend({}, {
    TheNumber() {
      return 42
    }
  })
  t.throws(() => tell.TheNumber())
})

test('add method with wrong type', t => {
  const tell = base.Extend({
    TheTruth() {
      return [[], false_]
    }
  }, {
    TheTruth() {
      return true_
    }
  })
  t.throws(() => tell.TheTruth())
  const worker = base.Extend({
    Echo() {
      return [[base, base], base]
    }
  }, {
    Echo(x) {
      return x
    }
  })
  t.throws(() => worker.Echo(base))
})

test('use `this` in type', t => {
  const expect = base.Extend({
    Type() {
      return [[], base]
    },
    WithType() {
      return [[base], this]
    },
    Echo() {
      return [[this.Type()], this.Type()]
    },
  }, {
    Type: null,
    WithType(type) {
      return this.Extend({}, {
        Type() {
          return type
        }
      })
    },
    Echo(x) {
       return x
    }
  })
  t.throws(() => expect.WithType(true_).Echo(false_))
})

test('call unimplemented method', t => {
  const worker = base.Extend({
    Echo() {
      return [[base], base]
    }
  }, {
    Echo: null
  })
  t.throws(() => worker.Echo(base))
})

test('override method with wrong type', t => {
  const expect = base.Extend({
    BaseReturnFalse() {
      return [[base], false_]
    }
  }, {
    BaseReturnFalse(b) {
      return false_
    }
  })
  const shouldFail = () => expect.Extend({
    BaseReturnFalse() {
      return [[true_], false_]
    }
  }, {})
  t.throws(shouldFail)
  const shouldFail2 = () => expect.Extend({
    BaseReturnFalse() {
      return [[base], base]
    }
  }, {})
  t.throws(shouldFail2)
  const shouldFail3 = () => expect.Extend({
    BaseReturnFalse() {
      return [[], false_]
    }
  }, {})
  t.throws(shouldFail3)
})

test('flexible constraint', t => {
  const objectWithType = base.Extend({
    Type() {
      return [[], base]
    },
    Of() {
      return [[base], this]
    }
  }, {
    Type: null,
    Of(type) {
      return this.Extend({}, {
        Type() {
          return type
        }
      })
    }
  })
  const typeChecker = base.Extend({
    Check() {
      return [
        [objectWithType], objectWithType,
        (obj) => [[obj.Type(), true_]], (obj) => [[obj.Type(), false_]]
      ]
    }
  }, {
    Check(obj) {
      return obj
    }
  })
  t.throws(() => typeChecker.Check(objectWithType.Of(false_)))
  t.throws(() => typeChecker.Check(objectWithType.Of(true_)))
})
