import test from 'ava'
import { just, nothing, default as optional } from './optional'
import number from './number'
import { true_, false_ } from './boolean'

test('nothing stays nothing', t => {
  t.is(nothing.Of(number).MapTo(number).Extend({}, {
    Do(n) {
      return just.WithValue(n.Add(number.Native(1)))
    }
  }).Run().Is(nothing), true_)
})

test('just could map to nothing', t => {
  t.is(just.Of(number).WithValue(number.Native(42)).MapTo(number).Extend({}, {
    Do(n) {
      return nothing.Of(number)
    }
  }).Run().Is(nothing), true_)
})

test('just could stay just', t => {
  t.is(just.Of(number).WithValue(number.Native(42)).MapTo(number).Extend({}, {
    Do(n) {
      return just.Of(number).WithValue(n.Add(number.Native(1)))
    }
  }).Run().Is(just), true_)
})

test('use `Extract` to convert an optional back', t => {
  t.is(number.Equal().Between(
    just.Of(number).WithValue(number.Native(42)).Extract(number.Native(43)),
    number.Native(42),
  ), true_)
  t.is(number.Equal().Between(
    nothing.Of(number).Extract(number.Native(43)),
    number.Native(43),
  ), true_)
})
