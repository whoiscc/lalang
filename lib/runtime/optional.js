import base from './base'
import equal from './equal'
import cond from './cond'
import { default as boolean, false_ } from './boolean'

const optional = base.Extend({
  Type() {
    return [[], base]
  },
  Of() {
    return [[base], this]
  },
  MapTo() {
    return [
      [base], mappedOptional, () => [],
      (mapped, [targetType]) => [
        [mapped.OriginalType(), this.Type()],
        [mapped.TargetType(), targetType],
      ]
    ]
  },
  Extract() {
    return [[this.Type()], this.Type()]
  }
}, {
  Type: null,
  Of(type) {
    return this.Extend({}, {
      Type() {
        return type
      }
    })
  },
  Extract: null,
})

let _ = {}

const nothing = optional.Extend({}, {
  MapTo(targetType) {
    return _.mappedNothing.Of(this.Type(), targetType)
  },
  Extract(fallback) {
    return fallback
  }
})

const just = optional.Extend({
  Value() {
    return [[], this.Type()]
  },
  WithValue() {
    return [[this.Type()], this]
  },
}, {
  Value: null,
  WithValue(value) {
    return this.Extend({}, {
      Value() {
        return value
      }
    })
  },
  MapTo(targetType) {
    return _.mappedJust.Of(this.Type(), targetType).WithValue(this.Value())
  },
  Extract(fallback) {
    return this.Value()
  }
})

const mappedOptional = base.Extend({
  OriginalType() {
    return [[], base]
  },
  TargetType() {
    return [[], base]
  },
  Of() {
    return [[base, base], this]
  },
  Do() {
    return [
      [this.OriginalType()], optional,
      () => [], (optional) => [[optional.Type(), this.TargetType()]]
    ]
  },
  Run() {
    return [
      [], optional, () => [],
      (optional) => [[optional.Type(), this.TargetType()]]
    ]
  }
}, {
  OriginalType: null,
  TargetType: null,
  Of(original, target) {
    return this.Extend({}, {
      OriginalType() {
        return original
      },
      TargetType() {
        return target
      }
    })
  },
  Do: null,
  Run: null
})

const mappedJust = mappedOptional.Extend({
  Value() {
    return [[], this.TargetType()]
  },
  WithValue() {
    return [[this.TargetType()], this]
  }
}, {
  Value: null,
  WithValue(value) {
    return this.Extend({}, {
      Value() {
        return value
      }
    })
  },
  Run() {
    return this.Do(this.Value())
  }
})

const mappedNothing = mappedOptional.Extend({}, {
  Run() {
    return nothing.Of(this.TargetType())
  }
})

_.just = just
_.nothing = nothing
_.mappedJust = mappedJust
_.mappedNothing = mappedNothing

export default optional
export { just, nothing }
