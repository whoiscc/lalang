import base from './base'
import { default as optional, just, nothing } from './optional'
import cond from './cond'

const cast = base.Extend({
  To() {
    return [[], base]
  },
  Of() {
    return [[base], this]
  },
  On() {
    return [
      [base], optional, () => [],
      (res) => [[res.Type(), this.To()]]
    ]
  }
}, {
  From: null,
  To: null,
  Of(to) {
    return this.Extend({}, {
      To() {
        return to
      }
    })
  },
  On(object) {
    const this_ = this
    return cond.Of(optional).Extend({}, {
      If() {
        return object.Is(this_.To())
      },
      Then() {
        return just.Of(this_.To()).WithValue(object)
      },
      Else() {
        return nothing.Of(this_.To())
      }
    }).Run()
  }
})

export default cast
