import test from './test'
import string from './string'
import number from './number'
import { just, nothing } from './optional'

test.Init('compare string').Extend({}, {
  Do() {
    this.Expect(string.Native('cowsay')).ToEqual(string.Native('cowsay'))
      .With(string.Equal())
    this.Expect(string.Native('cowsay')).ToNotEqual(string.Native('catsay'))
      .With(string.Equal())
  }
}).Run()

test.Init('slice string').Extend({}, {
  Do() {
    this.Expect(
      string.Native('cowsay').Slice(number.Native(3), number.Native(6))
    ).ToEqual(string.Native('say')).With(string.Equal())
  }
}).Run()

test.Init('string length').Extend({}, {
  Do() {
    this.Expect(string.Native('cowsay').Length()).ToEqual(number.Native(6))
      .With(number.Equal())
  }
}).Run()

test.Init('string at').Extend({}, {
  Do() {
    this.Expect(string.Native('cowsay').At(number.Native(6))).ToBe(nothing)
    const justC = string.Native('cowsay').At(number.Native(0))
    this.Expect(justC).ToBe(just)
    this.Expect(justC.Extract(string.Native(''))).ToEqual(string.Native('c'))
      .With(string.Equal())
  }
}).Run()
