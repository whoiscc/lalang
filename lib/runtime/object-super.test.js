import test from 'ava'
import base from './base'
import { true_ } from './boolean'

test('call parent version of method via `Super`', t => {
  const tell = base.Extend({
    TheNumber() {
      return [[], '_any']
    }
  }, {
    TheNumber() {
      return 42
    }
  })
  const tellPlusOne = tell.Extend({}, {
    TheNumber() {
      return this.Super().TheNumber() + 1
    }
  })
  t.deepEqual(tellPlusOne.TheNumber(), 43)
})

test('`this` always reference to caller', t => {
  const person = base.Extend({
    Name() {
      return [[], '_any']
    },
    SayHi() {
      return [[], '_any']
    },
    WithName() {
      return [['_any'], this]
    }
  }, {
    Name: null,
    WithName(name) {
      return this.Extend({}, {
        Name() {
          return name
        },
        SayHi() {
          return `${this.Super().SayHi.call(this)}!`
        }
      })
    },
    SayHi() {
      return `Hello, I'm ${this.Name()}`
    }
  })
  t.deepEqual(person.WithName('cowsay').SayHi(), 'Hello, I\'m cowsay!')
})

test('call inherited method via `Super`', t => {
  const dummy = base.Extend({
    Foo() {
      return [[], base]
    }
  }, {
    Foo() {
      return this.Super().Extend({}, {})
    }
  })
  t.is(dummy.Foo().Is(dummy), true_)
})
