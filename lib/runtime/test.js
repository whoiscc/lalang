import base from './base'
import avaTest from 'ava'
import { true_, false_ } from './boolean'
import equal from './equal'

let expect

const test = base.Extend({
  Init() {
    return [['_any'], this]
  },
  Do() {
    return [[], '_any']
  },
  Run() {
    return [[], '_any']
  },
  Expect() {
    return [[base], expect]
  },
  _Finish() {
    return [[], '_any']
  }
}, {
  Init(name) {
    let finish
    let promisedFinish = new Promise(resolve => {
      finish = () => resolve()
    })
    const promisedAvaT = new Promise(resolve => {
      avaTest(name, async t => {
        resolve(t)
        await promisedFinish
      })
    })
    return this.Extend({}, {
      Expect(tested) {
        return expect.Extend({}, {
          Tested() {
            return tested
          },
          AvaT() {
            return promisedAvaT
          }
        })
      },
      _Finish() {
        return finish()
      }
    })
  },
  Do: null,
  Run() {
    this.Do()
    this._Finish()
  },
})

const _waitForEqualType = base.Extend({
  Tested() {
    return [[], base]
  },
  Other() {
    return [[], base]
  },
  With() {
    return [[equal], '_any', (eq) => [
      [this.Tested(), eq.Type()],
      [this.Other(), eq.Type()]
    ]]
  },
  Of() {
    return [[base, base], this]
  }
}, {
  Tested: null,
  With: null,
  Of(tested, other) {
    return this.Extend({}, {
      Tested() {
        return tested
      },
      Other() {
        return other
      }
    })
  }
})

expect = base.Extend({
  Tested() {
    return [[], base]
  },
  AvaT() {
    return [[], '_any']
  },
  ToBe() {
    return [[base], '_any']
  },
  ToEqual() {
    return [
      [base], _waitForEqualType, () => [],
      (waitForEq, [other]) => [
        [waitForEq.Tested(), this.Tested()],
        [waitForEq.Other(), other]
      ]
    ]
  },
  ToNotEqual() {
    return [
      [base], _waitForEqualType, () => [],
      (waitForEq, [other]) => [
        [waitForEq.Tested(), this.Tested()],
        [waitForEq.Other(), other]
      ]
    ]
  },
  IsTrue() {
    return [[], '_any']
  },
  IsFalse() {
    return [[], '_any']
  },
  TestEqual() {
    return [[base, equal], expect]
  }
}, {
  Tested: null,
  AvaT: null,
  IsTrue() {
    this.AvaT().then(t => t.is(this.Tested(), true_))
  },
  IsFalse() {
    this.AvaT().then(t => t.is(this.Tested(), false_))
  },
  ToBe(parent) {
    const this_ = this
    return expect.Extend({}, {
      Tested() {
        return this_.Tested().Is(parent)
      },
      AvaT() {
        return this_.AvaT()
      }
    }).IsTrue()
  },
  TestEqual(other, equal) {
    const this_ = this
    return expect.Extend({}, {
      Tested() {
        return equal.Between(this_.Tested(), other)
      },
      AvaT() {
        return this_.AvaT()
      }
    })
  },
  ToEqual(other) {
    const this_ = this
    return _waitForEqualType.Of(this.Tested(), other).Extend({}, {
      With(eq) {
        this_.TestEqual(other, eq).IsTrue()
      }
    })
  },
  ToNotEqual(other) {
    const this_ = this
    return _waitForEqualType.Of(this.Tested(), other).Extend({}, {
      With(eq) {
        this_.TestEqual(other, eq).IsFalse()
      }
    })
  }
})

export default test
