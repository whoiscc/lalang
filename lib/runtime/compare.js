import equal from './equal'
import boolean from './boolean'

const compare = equal.Extend({
  Equal() {
    return [[this.Type(), this.Type()], boolean]
  },
  LessThan() {
    return [[this.Type(), this.Type()], boolean]
  },
  LessEqual() {
    return [[this.Type(), this.Type()], boolean]
  }
}, {
  Equal: null,
  LessThan: null,
  LessEqual(a, b) {
    return this.LessThan(a, b).Or(this.Equal(a, b))
  },

  Between(a, b) {
    return this.Equal(a, b)
  }
})

export default compare
