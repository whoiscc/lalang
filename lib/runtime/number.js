import base from './base'
import { true_, false_ } from './boolean'

let compareOfNumber

const number = base.Extend({
  Equal() {
    return [[], compareOfNumber]
  },
  Compare() {
    return [[], compareOfNumber]
  },

  Native() {
    return [['_any'], this]
  },
  _N() {
    return [[], '_any']
  },

  Add() {
    return [[number], number]
  },
  Neg() {
    return [[], number]
  },
}, {
  Equal() {
    return compareOfNumber
  },
  Compare() {
    return compareOfNumber
  },
  Native(n) {
    return this.Extend({}, {
      _N() {
        return n
      }
    })
  },
  _N: null,
  Add(other) {
    return number.Native(this._N() + other._N())
  },
  Neg() {
    return number.Native(- this._N())
  }
})

import equal from './equal'
import compare from './compare'
compareOfNumber = compare.Of(number).Extend({}, {
  Equal(a, b) {
    return a._N() == b._N() ? true_ : false_
  },
  LessThan(a, b) {
    return a._N() < b._N() ? true_ : false_
  }
})

export default number
