import test from 'ava'
import cast from './cast'
import { just, nothing } from './optional'
import { true_ } from './boolean'
import number from './number'

test('get a just on a successful cast', t => {
  const justNumber = cast.Of(number).On(number.Native(42))
  t.is(justNumber.Is(just), true_)
  t.is(justNumber.Type().Is(number), true_)
})

test('get a nothing on a failed case', t => {
  const nothingNumber = cast.Of(number).On(true_)
  t.is(nothingNumber.Is(nothing), true_)
  t.is(nothingNumber.Type().Is(number), true_)
})
