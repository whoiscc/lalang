import base from '../base'
import cond from '../cond'

const _ = {}

const equal = base.Extend({
  Of() {
    return [[base], equal]
  },
  Type() {
    return [[], base]
  },
  Between() {
    return [[this.Type(), this.Type()], _.boolean]
  },
}, {
  Type: null,
  Of(type) {
    return this.Extend({}, {
      Type() {
        return type
      },
    })
  },
  Between: null,
})

export default equal
export { _ as _placeholder }
