import base from './base'
import equal from './equal'
import { default as boolean, true_, false_ } from './boolean'
import cast from './cast'
import cond from './cond'
import number from './number'
import { default as optional, just, nothing } from './optional'
import string from './string'
import test from './test'

export default {
  base, equal, boolean, true_, false_, cast, cond, number,
  optional, just, nothing, string, test
}
